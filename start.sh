#!/bin/sh


users=(jayant aditi dil rahul chavan pranav shalina sukhdeep swati payal)
plugins=(gravityformscli wordfence aceide so-css duplicator what-the-file contact-form-7 autoptimize wp-fastest-cache wp-super-cache duracelltomi-google-tag-manager)
key=<ENTER THE KEY>

#Adding users from our team
for user in ${users[@]}
do
	wp user create "$user" "$user@botbakery.io" --role=administrator --send-email
done

#Installing and activating commonly used plugins
for plugin in ${plugins[@]}
do
	wp plugin install $plugin --activate
done

#Installing and activating Gravity forms plugin using the key
wp gf install --key=$key --activate --force

