# How to use start.sh

## Run on your machine:
1. Go to your WordPress website directory and copy this file in that location.
2. Get the key for Gravity forms activation.
3. Open start.sh file in your favourite text editor.
4. Replace the `<ENTER THE KEY>` with the 'key' you got
5. Save this file.
6. Now `cd` to your WordPress website directory through your command line terminal.
7. Run `. start.sh`

## Run using SSH and SCP:
1. Get the key for Gravity forms activation.
2. Open start.sh file in your favourite text editor.
3. Replace the `<ENTER THE KEY>` with the 'key' you got
4. Save this file.
5. Open you terminal
6. Run:
```
    ssh username@wordpresswebsite
```
7. Enter the password required as `YOUR PASSWORD`
```
    username@wordpresswebsite's password: <YOUR PASSWORD>
```
**You have now entered into your website's remote server machine B-)**

8. Run `ls` in the command line and note the `name` of your website
8. Now `cd` to your WordPress website named as `name` through the command line
9. Run:
``` pwd ```
10. Copy the output. For ex:
```
/home/admin/website's_name
```
11. Press `Ctrl+D`
12. Now run from your local directory:
```
    scp start.sh username@wordpresswebsite:<your copied output from step 10>
```
13. Enter the password
14. You are done with copying!!!
15. Again repeat steps 6 to 8
16. Run `ls` to check whether `start.sh` is copied there
17. Run `. start.sh`
18. YAYYYY!!!!! You did great!

### You are done! :D